using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using NuGet.Versioning;
using NUnit.Framework;
using static System.Console;


namespace Vrange
{
    public static class TestHelper
    {
        public static string Strip(string s)
        {
            return Regex.Replace(s, @"[^0-9a-zA-Z\.]+", ",");
        }
    }

    [TestFixture]
    public class TestVersionMatcher
    {
        [Test]
        public void test_simple_input()
        {
            string input_constraints = @"[
            {
                ""range"": ""(4.1.3,)"",
                ""version"": ""2.0.0""
            },
            {
                ""range"": ""(4.1.3,4.1.3)"",
                ""version"": ""1.1.0""
            },
            {
                ""range"": ""(4.1.3,4.1.4]"",
                ""version"": ""1.0""
            },
            {
                ""range"": ""(4.1.3,4.1.4]"",
                ""version"": ""4.1.3""
            },
            {
                ""range"": ""(4.1.3,4.1.4]"",
                ""version"": ""4.1.4""
            },
            {
                ""range"": ""[1.3.2,1.5)"",
                ""version"": ""1.4.1""
            },
            {
                ""range"": ""[1.3.2-alpha,1.5-beta)"",
                ""version"": ""1.3.3""
            },
            {
                ""range"": ""1.3.2-alpha_:*$()#"",
                ""version"": ""1.3.3""
            }
            ]";

            string expected = @"[
            {
                ""range"": ""(4.1.3,)"",
                ""version"": ""2.0.0"",
                ""satisfies"": false
            },
            {
                ""range"": ""(4.1.3,4.1.3)"",
                ""version"": ""1.1.0"",
                ""satisfies"": false
            },
            {
                ""range"": ""(4.1.3,4.1.4]"",
                ""version"": ""1.0"",
                ""satisfies"": false
            },
            {
                ""range"": ""(4.1.3,4.1.4]"",
                ""version"": ""4.1.3"",
                ""satisfies"": false
            },
            {
                ""range"": ""(4.1.3,4.1.4]"",
                ""version"": ""4.1.4"",
                ""satisfies"": true
            },
            {
                ""range"": ""[1.3.2,1.5)"",
                ""version"": ""1.4.1"",
                ""satisfies"": true
            },
            {
                ""range"": ""[1.3.2-alpha,1.5-beta)"",
                ""version"": ""1.3.3"",
                ""satisfies"": true
            },
            {
                ""range"": ""1.3.2-alpha_:*$()#"",
                ""version"": ""1.3.3"",
                ""error"": ""malformed range""
            }
            ]";

            Assert.AreEqual(TestHelper.Strip(expected),
                TestHelper.Strip(VersionMatcher.checkVersion(input_constraints)));
        }

        [Test]
        public void test_pre_post()
        {
            string input_constraints = @"[
            {
                ""range"": ""(4.1.3-alpha,)"",
                ""version"": ""4.1.3-beta""
            },
            {
                ""range"": ""[4.1.3-beta,4.1.3-beta2)"",
                ""version"": ""4.1.3-beta""
            },
            {
                ""range"": ""[0.1.1,2.1.4]"",
                ""version"": ""2.1.4-BETA""
            },
            {
                ""range"": ""[0.1.1,2.1.4]"",
                ""version"": ""0.1.1-alpha""
            }
            ]";

            string expected = @"[
            {
                ""range"": ""(4.1.3-alpha,)"",
                ""version"": ""4.1.3-beta"",
                ""satisfies"": true
            },
            {
                ""range"": ""[4.1.3-beta,4.1.3-beta2)"",
                ""version"": ""4.1.3-beta"",
                ""satisfies"": true
            },
            {
                ""range"": ""[0.1.1,2.1.4]"",
                ""version"": ""2.1.4-BETA"",
                ""satisfies"": true
            },
            {
                ""range"": ""[0.1.1,2.1.4]"",
                ""version"": ""0.1.1-alpha"",
                ""satisfies"": false
            }
            ]";

            Assert.AreEqual(TestHelper.Strip(expected),
                TestHelper.Strip(VersionMatcher.checkVersion(input_constraints)));
        }

        [Test]
        public void test_split_constraint()
        {
            var constraint = "(4.1.3-alpha,4.0.0),   (5.4, 5.7.9), [1.3, 4.6], [100,), 200, 400, [100], 2.1 ";
            var actual = VersionMatcher.splitIntoDisjuncts(constraint);

            Assert.AreEqual(actual.Count, 8);

            List<VersionRange> expected = new List<VersionRange>
            {
                VersionRange.Parse("(4.1.3-alpha,4.0.0)"),
                VersionRange.Parse("(5.4, 5.7.9)"),
                VersionRange.Parse("[1.3, 4.6]"),
                VersionRange.Parse("[100,)"),
                VersionRange.Parse("200"),
                VersionRange.Parse("400"),
                VersionRange.Parse("[100]"),
                VersionRange.Parse("2.1"),
            };
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void test_multi_ranges()
        {
            string input_constraints = @"[
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5, 5.7.9)"",
                ""version"": ""5.5""
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5.4, 5.7.9)"",
                ""version"": ""4.0.0""
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5.4, 5.7.9)"",
                ""version"": ""5.2.1""
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5, 5.7.9)"",
                ""version"": ""4.1.4""
            },
            {
                ""range"": """",
                ""version"": ""4.1.4""
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5, 5.7.9), [1.1, 1.2], [3.4, 2.1]"",
                ""version"": ""1.1""
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5, 5.7.9), [1.1, 1.2], [3.4, 2.1]"",
                ""version"": """"
            },
            ]";

            var expected = @"[
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5, 5.7.9)"",
                ""version"": ""5.5"",
                ""satisfies"": true
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5.4, 5.7.9)"",
                ""version"": ""4.0.0"",
                ""satisfies"": false
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5.4, 5.7.9)"",
                ""version"": ""5.2.1"",
                ""satisfies"": false
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5, 5.7.9)"",
                ""version"": ""4.1.4"",
                ""satisfies"": false
            },
            {
                ""range"": """",
                ""version"": ""4.1.4"",
                ""error"": ""malformed range ""
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5, 5.7.9), [1.1, 1.2], [3.4, 2.1]"",
                ""version"": ""1.1"",
                ""satisfies"": true
            },
            {
                ""range"": ""(4.1.3-alpha,4.0.0), (5, 5.7.9), [1.1, 1.2], [3.4, 2.1]"",
                ""version"": """",
                ""error"": ""malformed version""
            }
            ]";

            Assert.AreEqual(TestHelper.Strip(expected),
                TestHelper.Strip(VersionMatcher.checkVersion(input_constraints)));
        }

        [Test]
        public void test_invalid_input()
        {
            var exceptionThrown = false;
            try
            {
                VersionMatcher.checkVersion("");
            }
            catch (VrangeException)
            {
                exceptionThrown = true;
            }

            Assert.True(exceptionThrown);
        }

        [Test]
        public void test_empty_array()
        {
            Assert.AreEqual("[]", VersionMatcher.checkVersion("[]"));
        }
    }
}