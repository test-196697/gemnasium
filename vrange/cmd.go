package vrange

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"os/exec"
)

// RegisterCmd registers a resolver that is based on a CLI.
func RegisterCmd(syntaxName string, path string, args ...string) error {
	if _, err := os.Stat(path); err != nil {
		return err
	}
	r := &CmdResolver{Path: path, Args: args}
	Register(syntaxName, r)
	return nil
}

// CmdResolver is used to implement a Resolver using a CLI.
type CmdResolver struct {
	Path string   // Path is the path of the command to run.
	Args []string // Args are the command arguments.
	Dir  string   // Dir is the working directory.
}

// Resolve evaluates queries.
func (r CmdResolver) Resolve(queries []Query) (*ResultSet, error) {

	// create input document
	tmpfile, err := ioutil.TempFile("/tmp", "vrange_queries")
	if err != nil {
		return nil, err
	}
	defer os.Remove(tmpfile.Name())
	if err := json.NewEncoder(tmpfile).Encode(queries); err != nil {
		return nil, err
	}
	if err := tmpfile.Close(); err != nil {
		return nil, err
	}

	// run command, read output
	args := append(r.Args, tmpfile.Name())
	cmd := exec.Command(r.Path, args...)
	cmd.Dir = r.Dir
	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, err
	}

	// decode results
	results := []struct {
		Version   string
		Range     string
		Satisfies bool
		Error     string
	}{}
	if err := json.Unmarshal(out, &results); err != nil {
		return nil, err
	}

	// build result set
	set := make(ResultSet)
	for _, result := range results {
		var err error = nil
		if result.Error != "" {
			err = errors.New(result.Error)
		}
		query := Query{Version: result.Version, Range: result.Range}
		set.Set(query, result.Satisfies, err)
	}
	return &set, nil
}
