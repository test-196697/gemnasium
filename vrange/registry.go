package vrange

import (
	"sort"
	"sync"
)

var resolversMu sync.Mutex
var resolvers = make(map[string]Resolver)

// Register registers a resolver with a name.
func Register(name string, resolver Resolver) {
	resolversMu.Lock()
	defer resolversMu.Unlock()
	if _, dup := resolvers[name]; dup {
		panic("Register called twice for name " + name)
	}
	resolvers[name] = resolver
}

// Lookup looks for a resolver with given name.
func Lookup(name string) Resolver {
	resolversMu.Lock()
	defer resolversMu.Unlock()
	resolver, _ := resolvers[name]
	return resolver
}

// Resolvers returns a sorted list of the names of the registered resolvers.
func Resolvers() []string {
	resolversMu.Lock()
	defer resolversMu.Unlock()
	var list []string
	for name := range resolvers {
		list = append(list, name)
	}
	sort.Strings(list)
	return list
}
