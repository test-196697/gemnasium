package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/remediate"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/golang"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/composer"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/conan"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/go"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/mvnplugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/npm"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/nuget"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
)

const (
	flagTargetDir        = "target-dir"
	flagArtifactDir      = "artifact-dir"
	flagRemediate        = "remediate"
	flagRemediateTimeout = "remediate-timeout"

	flagVrangeDir       = "vrange-dir"
	flagVrangeGemCmd    = "vrange-gem-cmd"
	flagVrangeMavenCmd  = "vrange-maven-cmd"
	flagVrangeNugetCmd  = "vrange-nuget-cmd"
	flagVrangeNpmCmd    = "vrange-npm-cmd"
	flagVrangePhpCmd    = "vrange-php-cmd"
	flagVrangePythonCmd = "vrange-python-cmd"
	flagVrangeGoCmd     = "vrange-go-cmd"
	flagVrangeConanCmd  = "vrange-conan-cmd"

	defaultTimeoutRemediate = 5 * time.Minute
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = metadata.AnalyzerVersion
	app.Author = metadata.AnalyzerVendor
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	app.Commands = []cli.Command{runCommand()}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func runCommand() cli.Command {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:   flagTargetDir,
			Usage:  "Target directory",
			EnvVar: command.EnvVarTargetDir + "," + command.EnvVarCIProjectDir,
		},
		cli.StringFlag{
			Name:   flagArtifactDir,
			Usage:  "Artifact directory",
			EnvVar: command.EnvVarArtifactDir + "," + command.EnvVarCIProjectDir,
		},
		cli.BoolTFlag{
			Name:   flagRemediate,
			Usage:  "Remediate vulnerabilities",
			EnvVar: "DS_REMEDIATE",
		},
		cli.DurationFlag{
			Name:   flagRemediateTimeout,
			EnvVar: "DS_REMEDIATE_TIMEOUT",
			Usage:  "Time limit for vulnerabilities auto-remediation",
			Value:  defaultTimeoutRemediate,
		},

		// vrange CLIs
		cli.StringFlag{
			Name:   flagVrangeDir,
			Usage:  "Path of the vrange directory",
			EnvVar: "VRANGE_DIR",
			Value:  "vrange",
		},
		cli.StringFlag{
			Name:   flagVrangeGemCmd,
			Usage:  "vrange command for Rubygem",
			EnvVar: "VRANGE_GEM_CMD",
			Value:  "gem/vrange.rb",
		},
		cli.StringFlag{
			Name:   flagVrangeMavenCmd,
			Usage:  "vrange command for Maven",
			EnvVar: "VRANGE_MAVEN_CMD",
			Value:  "semver/vrange-" + runtime.GOOS + " maven",
		},
		cli.StringFlag{
			Name:   flagVrangeNugetCmd,
			Usage:  "vrange command for NuGet",
			EnvVar: "VRANGE_NUGET_CMD",
			Value:  "nuget/vrange-" + runtime.GOOS,
		},
		cli.StringFlag{
			Name:   flagVrangeNpmCmd,
			Usage:  "vrange command for npm",
			EnvVar: "VRANGE_NPM_CMD",
			Value:  "npm/rangecheck.js",
		},
		cli.StringFlag{
			Name:   flagVrangePhpCmd,
			Usage:  "vrange command for PHP",
			EnvVar: "VRANGE_PHP_CMD",
			Value:  "php/rangecheck.php",
		},
		cli.StringFlag{
			Name:   flagVrangePythonCmd,
			Usage:  "vrange command for Python",
			EnvVar: "VRANGE_PYTHON_CMD",
			Value:  "python/rangecheck.py",
		},
		cli.StringFlag{
			Name:   flagVrangeGoCmd,
			Usage:  "vrange command for go",
			EnvVar: "VRANGE_GO_CMD",
			Value:  "semver/vrange-" + runtime.GOOS + " npm",
		},
		cli.StringFlag{
			Name:   flagVrangeConanCmd,
			Usage:  "vrange command for conan",
			EnvVar: "VRANGE_CONAN_CMD",
			Value:  "npm/rangecheck.js",
		},
	}

	flags = append(flags, cacert.NewFlags()...)
	flags = append(flags, search.NewFlags()...)
	flags = append(flags, scanner.Flags()...)
	flags = append(flags, pathfilter.MakeFlags("DS_")...)

	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			startTime := issue.ScanTime(time.Now())

			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// register version range resolvers
			if err := registerResolvers(c); err != nil {
				return err
			}

			// parse excluded paths
			filter, err := pathfilter.NewFilter(c)
			if err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flagTargetDir))
			if err != nil {
				return err
			}

			// search
			searchOpts := search.NewOptions(c)
			matchPath, err := search.New(plugin.Match, searchOpts).Run(targetDir)
			if err != nil {
				if e, ok := err.(search.ErrNotFound); ok {
					log.Warn(e)
					return nil
				}

				return err
			}
			log.Infof("Found project in %s\n", matchPath)

			// scan target directory
			scanner, err := scanner.NewScanner(c)
			if err != nil {
				return err
			}
			result, err := scanner.ScanDir(targetDir)
			if err != nil {
				return err
			}

			// convert to generic report
			var prependPath = "" // empty because the analyzer scans the root directory
			var report = convert.ToReport(result, prependPath, &startTime)

			// remediate vulnerabilities
			if c.BoolT(flagRemediate) {
				if !isGitClone(targetDir) {
					log.Warn("auto-remediation requires a valid git directory") // don't fail
				} else {
					var t = c.Duration(flagRemediateTimeout)
					ctx, cancel := context.WithTimeout(context.Background(), t)
					defer cancel()
					report.Remediations = remediations(ctx, result...)
				}
			}

			// filter paths, sort
			report.ExcludePaths(filter.IsExcluded)
			report.Sort()

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), command.ArtifactNameDependencyScanning)
			f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f.Close()
			enc := json.NewEncoder(f)
			enc.SetIndent("", "  ")
			return enc.Encode(report)
		},
	}
}

func registerResolvers(c *cli.Context) error {
	// register CLI-based resolvers
	syntaxToFlag := map[string]string{
		"gem":    flagVrangeGemCmd,
		"maven":  flagVrangeMavenCmd,
		"nuget":  flagVrangeNugetCmd,
		"npm":    flagVrangeNpmCmd,
		"php":    flagVrangePhpCmd,
		"python": flagVrangePythonCmd,
		"conan":  flagVrangeConanCmd,
	}
	for syntax, flag := range syntaxToFlag {
		cmd := strings.SplitN(c.String(flag), " ", 2)
		path := filepath.Join(c.String(flagVrangeDir), cmd[0])
		args := cmd[1:]
		if err := vrange.RegisterCmd(syntax, path, args...); err != nil {
			return err
		}
	}

	// register native resolvers
	vrange.Register("go", &golang.Resolver{})

	return nil
}

// remediations attempts to cure affected dependency files and returns remediations.
func remediations(ctx context.Context, scanFiles ...scanner.File) []issue.Remediation {
	var allRems = make([]issue.Remediation, 0)
	for _, file := range scanFiles {
		// attempt to cure
		cures, err := remediate.Remediate(ctx, file)
		switch err {
		case nil:
			// proceed
		case context.DeadlineExceeded:
			// report timeout and proceed
			log.Error("timeout exceeded during auto-remediation")
		default:
			// report error and move on to the next dependency file;
			// dependency scanning must not fail when remediation fails
			log.Print(err)
			continue
		}

		// convert cures to remediations;
		// it can't be extracted out of this loop because dependency file is required.
		var rems = make([]issue.Remediation, len(cures))
		for i, cure := range cures {
			var refs = make([]issue.Ref, len(cure.Affections))
			for j, a := range cure.Affections {
				// HACK convert to an issue to create a reference
				var vuln = convert.VulnerabilityConverter{
					FilePath:   file.Path,
					Advisory:   a.Advisory,
					Dependency: a.Dependency,
				}.Issue()

				refs[j] = issue.NewRef(vuln)
			}
			rems[i] = issue.Remediation{
				Fixes:   refs,
				Summary: cure.Summary,
				Diff:    base64.StdEncoding.EncodeToString(cure.Diff),
			}
		}
		allRems = append(allRems, rems...)
	}
	return allRems
}

func isGitClone(dir string) bool {
	cmd := exec.Command("git", "-C", dir, "status")
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	return err == nil
}
