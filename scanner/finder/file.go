package finder

import (
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// File is a dependency file that has been found.
type File struct {
	Path    string        // Path is the path relative to the root directory.
	RootDir string        // RootDir is the directory where the finder has looked for files.
	Parser  parser.Parser // Parser is the dependency file parser compatible with the file.
}
