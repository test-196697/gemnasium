package convert

import (
	"bytes"
	"encoding/json"
	"os"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestConvert(t *testing.T) {

	// advisories
	pgAdvisory := advisory.Advisory{
		UUID:        "7d9ba955-fd99-4503-936e-f6833768f76e",
		Package:     advisory.Package{Type: "gem", Name: "pg"},
		Identifier:  "CVE-1234",
		Title:       "Regular Expression Denial of Service",
		Description: "Xyz is vulnerable to ReDoS in the Xyz parameter.",
		Solution:    "Upgrade to latest version.",
		Links:       []string{"https://security.io/advisories/119", "https://security.io/advisories/117", "https://security.io/advisories/118"},
		URL:         "https://gitlab.com/gemnasium-db/-/blob/master/gem/pg/CVE-1234.yml",
	}

	// expected scan information
	scan := issue.Scan{
		Scanner: issue.ScannerDetails{
			ID:   "gemnasium",
			Name: "Gemnasium",
			URL:  "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
			Vendor: issue.Vendor{
				Name: "GitLab",
			},
			Version: metadata.ScannerVersion,
		},
		Type:   "dependency_scanning",
		Status: issue.StatusSuccess,
	}

	// test cases
	tcs := []struct {
		name           string
		envDepPathMode string
		files          []scanner.File
		report         *issue.Report
	}{
		{
			"two files, no graph",
			"",
			[]scanner.File{
				{
					Path:           "rails/Gemfile.lock",
					PackageManager: issue.PackageManagerBundler,
					PackageType:    "gem",
					Packages: []parser.Package{
						{Name: "pg", Version: "0.8.0"},
						{Name: "puma", Version: "2.16.0"},
					},
					Affections: []scanner.Affection{
						{
							Advisory:   pgAdvisory,
							Dependency: parser.Package{Name: "pg", Version: "0.8.0"},
						},
					},
				},
				{
					Path:           "node/yarn.lock",
					PackageManager: issue.PackageManagerYarn,
					PackageType:    "npm",
					Packages: []parser.Package{
						{Name: "acorn", Version: "4.0.4"},
						{Name: "acorn", Version: "3.3.0"},
						{Name: "acorn", Version: "4.0.11"},
						{Name: "@angular/animations", Version: "4.4.6"},
					},
				},
			},
			&issue.Report{
				Version: issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{
					{
						Category:    metadata.Type,
						Scanner:     metadata.IssueScanner,
						Name:        "Regular Expression Denial of Service",
						Message:     "Regular Expression Denial of Service in pg",
						Description: "Xyz is vulnerable to ReDoS in the Xyz parameter.",
						CompareKey:  "app/rails/Gemfile.lock:pg:gemnasium:7d9ba955-fd99-4503-936e-f6833768f76e",
						Severity:    issue.SeverityLevelUnknown,
						Solution:    "Upgrade to latest version.",
						Location: issue.Location{
							File: "app/rails/Gemfile.lock",
							Dependency: &issue.Dependency{
								Package: issue.Package{Name: "pg"},
								Version: "0.8.0",
							},
						},
						Identifiers: []issue.Identifier{
							{
								Type:  "gemnasium",
								Name:  "Gemnasium-7d9ba955-fd99-4503-936e-f6833768f76e",
								Value: "7d9ba955-fd99-4503-936e-f6833768f76e",
								URL:   "https://gitlab.com/gemnasium-db/-/blob/master/gem/pg/CVE-1234.yml",
							},
							{
								Type:  "cve",
								Name:  "CVE-1234",
								Value: "CVE-1234",
								URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1234",
							},
						},
						Links: []issue.Link{
							{URL: "https://security.io/advisories/117"},
							{URL: "https://security.io/advisories/118"},
							{URL: "https://security.io/advisories/119"},
						},
					},
				},
				Remediations: []issue.Remediation{},
				DependencyFiles: []issue.DependencyFile{
					{
						Path:           "app/rails/Gemfile.lock",
						PackageManager: "bundler",
						Dependencies: []issue.Dependency{
							{Package: issue.Package{Name: "pg"}, Version: "0.8.0"},
							{Package: issue.Package{Name: "puma"}, Version: "2.16.0"},
						},
					},
					{
						Path:           "app/node/yarn.lock",
						PackageManager: "yarn",
						Dependencies: []issue.Dependency{
							{Package: issue.Package{Name: "acorn"}, Version: "4.0.4"},
							{Package: issue.Package{Name: "acorn"}, Version: "3.3.0"},
							{Package: issue.Package{Name: "acorn"}, Version: "4.0.11"},
							{Package: issue.Package{Name: "@angular/animations"}, Version: "4.4.6"},
						},
					},
				},
				Scan: scan,
			},
		},

		{
			"single file, graph",
			"",
			[]scanner.File{
				{
					Path:           "rails/Gemfile.lock",
					PackageManager: issue.PackageManagerBundler,
					PackageType:    "gem",
					Packages: []parser.Package{
						{Name: "postgres-copy", Version: "1.5.0"},
						{Name: "pg", Version: "0.8.0"},
						{Name: "activerecord", Version: "5.1"},
					},
					Dependencies: []parser.Dependency{
						{
							To: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
						},
						{
							From: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
							To:   &parser.Package{Name: "pg", Version: "0.8.0"},
						},
						{
							From: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
							To:   &parser.Package{Name: "activerecord", Version: "5.1"},
						},
					},
					Affections: []scanner.Affection{
						{
							Advisory:   pgAdvisory,
							Dependency: parser.Package{Name: "pg", Version: "0.8.0"},
						},
					},
				},
			},
			&issue.Report{
				Version: issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{
					{
						Category:    metadata.Type,
						Scanner:     metadata.IssueScanner,
						Name:        "Regular Expression Denial of Service",
						Message:     "Regular Expression Denial of Service in pg",
						Description: "Xyz is vulnerable to ReDoS in the Xyz parameter.",
						CompareKey:  "app/rails/Gemfile.lock:pg:gemnasium:7d9ba955-fd99-4503-936e-f6833768f76e",
						Severity:    issue.SeverityLevelUnknown,
						Solution:    "Upgrade to latest version.",
						Location: issue.Location{
							File: "app/rails/Gemfile.lock",
							Dependency: &issue.Dependency{
								IID:     2,
								Package: issue.Package{Name: "pg"},
								Version: "0.8.0",
							},
						},
						Identifiers: []issue.Identifier{
							{
								Type:  "gemnasium",
								Name:  "Gemnasium-7d9ba955-fd99-4503-936e-f6833768f76e",
								Value: "7d9ba955-fd99-4503-936e-f6833768f76e",
								URL:   "https://gitlab.com/gemnasium-db/-/blob/master/gem/pg/CVE-1234.yml",
							},
							{
								Type:  "cve",
								Name:  "CVE-1234",
								Value: "CVE-1234",
								URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1234",
							},
						},
						Links: []issue.Link{
							{URL: "https://security.io/advisories/117"},
							{URL: "https://security.io/advisories/118"},
							{URL: "https://security.io/advisories/119"},
						},
					},
				},
				Remediations: []issue.Remediation{},
				DependencyFiles: []issue.DependencyFile{
					{
						Path:           "app/rails/Gemfile.lock",
						PackageManager: "bundler",
						Dependencies: []issue.Dependency{
							{
								IID:     1,
								Package: issue.Package{Name: "postgres-copy"},
								Version: "1.5.0",
							},
							{
								IID:     2,
								Package: issue.Package{Name: "pg"},
								Version: "0.8.0",
								DependencyPath: []issue.DependencyRef{
									{IID: 1},
								},
							},
							{
								IID:     3,
								Package: issue.Package{Name: "activerecord"},
								Version: "5.1",
							},
						},
					},
				},
				Scan: scan,
			},
		},

		{
			"single file, no affection, path to all",
			"all",
			[]scanner.File{
				{
					Path:           "rails/Gemfile.lock",
					PackageManager: issue.PackageManagerBundler,
					PackageType:    "gem",
					Packages: []parser.Package{
						{Name: "postgres-copy", Version: "1.5.0"},
						{Name: "activerecord", Version: "5.1"},
					},
					Dependencies: []parser.Dependency{
						{
							To: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
						},
						{
							From: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
							To:   &parser.Package{Name: "activerecord", Version: "5.1"},
						},
					},
				},
			},
			&issue.Report{
				Version:         issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{},
				Remediations:    []issue.Remediation{},
				DependencyFiles: []issue.DependencyFile{
					{
						Path:           "app/rails/Gemfile.lock",
						PackageManager: "bundler",
						Dependencies: []issue.Dependency{
							{
								IID:     1,
								Direct:  true,
								Package: issue.Package{Name: "postgres-copy"},
								Version: "1.5.0",
							},
							{
								IID:     2,
								Package: issue.Package{Name: "activerecord"},
								Version: "5.1",
								DependencyPath: []issue.DependencyRef{
									{IID: 1},
								},
							},
						},
					},
				},
				Scan: scan,
			},
		},

		{
			"single file, no affection, no path",
			"none",
			[]scanner.File{
				{
					Path:           "rails/Gemfile.lock",
					PackageManager: issue.PackageManagerBundler,
					PackageType:    "gem",
					Packages: []parser.Package{
						{Name: "postgres-copy", Version: "1.5.0"},
						{Name: "activerecord", Version: "5.1"},
					},
					Dependencies: []parser.Dependency{
						{
							To: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
						},
						{
							From: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
							To:   &parser.Package{Name: "activerecord", Version: "5.1"},
						},
					},
				},
			},
			&issue.Report{
				Version:         issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{},
				Remediations:    []issue.Remediation{},
				DependencyFiles: []issue.DependencyFile{
					{
						Path:           "app/rails/Gemfile.lock",
						PackageManager: "bundler",
						Dependencies: []issue.Dependency{
							{
								Package: issue.Package{Name: "postgres-copy"},
								Version: "1.5.0",
							},
							{
								Package: issue.Package{Name: "activerecord"},
								Version: "5.1",
							},
						},
					},
				},
				Scan: scan,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			// prepare input reader
			var input bytes.Buffer
			enc := json.NewEncoder(&input)
			enc.SetIndent("", "  ")
			if err := enc.Encode(tc.files); err != nil {
				t.Fatal(err)
			}
			r := bytes.NewReader(input.Bytes())

			// convert
			os.Setenv("DS_DEPENDENCY_PATH_MODE", tc.envDepPathMode)
			prependPath := "app"
			got, err := Convert(r, prependPath)
			if err != nil {
				t.Fatal(err)
			}

			// compare
			want := tc.report
			if !reflect.DeepEqual(want, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
			}
		})
	}
}
