package convert

import (
	"encoding/json"
	"io"
	"os"
	"time"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
)

const (
	identifierTypeGemnasiumUUID  = issue.IdentifierType("gemnasium")
	identifierPrefixGemnasiuUUID = "Gemnasium-"

	gemnasiumURL = "https://deps.sec.gitlab.com"

	// envDepPathMode is the name of the environment variable
	// that controls how dependency paths are rendered (EXPERIMENTAL)
	envDepPathMode = "DS_DEPENDENCY_PATH_MODE"
)

// Convert converts the output of the Gemnasium (list of dependency files) to a report.
func Convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	var result []scanner.File
	err := json.NewDecoder(reader).Decode(&result)
	if err != nil {
		return nil, err
	}
	return ToReport(result, prependPath, nil), nil
}

// ToReport converts dependency files returned by the Gemnasium scanner to a report.
func ToReport(scanFiles []scanner.File, prependPath string, startTime *issue.ScanTime) *issue.Report {
	// convert scanned files, collect vulnerabilities and dep. files
	vulns := []issue.Issue{}
	depfiles := make([]issue.DependencyFile, len(scanFiles))
	for i, scanFile := range scanFiles {
		cfg := Config{
			PrependPath: prependPath,
			DepPathMode: depPathModeForFile(scanFile),
		}

		// converted scanned file
		c := NewFileConverter(scanFile, cfg)
		vulns = append(vulns, c.Vulnerabilities()...)
		depfiles[i] = c.DependencyFile()
	}

	report := issue.NewReport()
	report.Vulnerabilities = vulns
	report.DependencyFiles = depfiles
	report.Scan.Scanner = metadata.ReportScanner
	report.Scan.Type = metadata.Type
	report.Scan.Status = issue.StatusSuccess
	if startTime != nil {
		report.Scan.StartTime = startTime
		endTime := issue.ScanTime(time.Now())
		report.Scan.EndTime = &endTime
	}

	return &report
}

func depPathModeForFile(f scanner.File) DepPathMode {
	if len(f.Dependencies) == 0 {
		// dependency graph information not available
		return DepPathModeNone
	}

	switch os.Getenv(envDepPathMode) {
	case "all":
		return DepPathModeAll
	case "none":
		return DepPathModeNone
	default:
		return DepPathModeAffected
	}
}
